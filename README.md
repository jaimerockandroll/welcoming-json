# referencia typingOnTime/Off:

Fijarse en los tiempos de sleep:

        return sleep(typingOnTime).then(() => {
            returntypingOnEvent(userId).then(()=>{
                return sleep(typingOffTime).then(() => {
                    return typingOffEvent(userId).then(()=>{
                        return sendSingleMessage(userId,single_message);
                    });
                });
            });
        });


# Ejemplo de Json

    {
      "step_one": {
    
        "message" : ["bienvenido... qué ves aquí?", "fíjate bien eh"],
        "message_button": "https://res.cloudinary.com/btygrd/image/upload/v1534955870/picture/unicornio.jpg",
        "buttons" : ["Un Unicornio", "Un Mojito"]
      },
    
      "un unicornio": {
    
        "message" : "Anda prueba otra vez",
        "message_button": "https://res.cloudinary.com/btygrd/image/upload/v1534955870/picture/unicornio.jpg",
        "buttons" : ["Un Mojito"]
      },
    
      "un mojito": {
    
        "message" : "Enhorabuena, Bienvenido al chiringuito!",
        "is_last_step": true,
        "next_command": "Chat"
      }
    
    }


## Reglas:

- "step_one" es obligatorio, es el punto de entrada y no se puede tocar
- Cada botón, a excepción del último tiene estos 3 campos:
    - message: puede ser un texto en comillas o un array (para más de un mensaje): ["mensaje1", "mensaje2"]. 
Notar que los corchetes van por fuera de las comillas
    - message_button: Puede ser un mensaje o una url con la imagen. Este campo aparece justo encima de los botons
    - buttons: debe ser un array OBLIGATORIAMENTE (aunque solo haya 1 botón)
- El último paso debe llevar los siguientes campos obligatoriamente:
    - "is_last_step" : true,
    - "next_command": indica el nuevo comando a disparar justo después del mensaje
    
